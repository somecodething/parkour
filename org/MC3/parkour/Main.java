package org.MC3.parkour;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;

public class Main extends JavaPlugin {
	public static Main instance;
	public static FileConfiguration config;
	public static FileConfiguration save;
	
	
	public void onEnable() {
		instance = this;
		config = this.getConfig();
		config.options().copyDefaults(true);
		saveConfig();
		reloadConfig();
		
		registerEvents(instance,
				new PlayerJoin(),
				new Parkour()
			);
	}

	public void onDisable() {
		Bukkit.getScheduler().cancelAllTasks();
	}
	
	public static void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
		for(Listener listener : listeners) {
			Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
		}
	}
	
	public static void sendActionBar(Player player, String message) {
	    IChatBaseComponent cbc = ChatSerializer.a("{\"text\": \"" + message + "\"}");
	    PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc,(byte) 2);
	    ((CraftPlayer) player).getHandle().playerConnection.sendPacket(ppoc);
	}
	
	public void onPlayerLeave(PlayerQuitEvent e) {
		Player player = e.getPlayer();
		Parkour.start.remove(player);
	}
}
