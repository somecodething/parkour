package org.MC3.parkour;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import me.Swedz.api.API;

public class PlayerJoin implements Listener {
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) throws Exception {
		Player player = e.getPlayer();
		PlayerInventory inv = player.getInventory();
		ItemStack air = new ItemStack(Material.AIR);
		ItemStack hub = API.getItemAPI().createItem(new ItemStack(Material.BED), ChatColor.GREEN + "Return to Hub", new String[] {}, null, false, true);
		ItemStack settings = API.getItemAPI().createItem(new ItemStack(Material.REDSTONE_COMPARATOR), ChatColor.GREEN + "Settings", new String[] {}, null, false, true);
		ItemStack cosmetics = API.getItemAPI().createItem(new ItemStack(Material.CHEST), ChatColor.GREEN + "Cosmetics", new String[] {}, null, false, true);
		ItemStack menu = API.getItemAPI().createItem(new ItemStack(Material.ENCHANTED_BOOK), ChatColor.GREEN + "Parkour Menu", new String[] {}, null, false, true);
		
		Location loc = new Location(Bukkit.getWorld("world"), 0.5, 65, 0.5);
		loc.setYaw(-180); loc.setPitch(0);
		player.teleport(loc);
		Main.config.set("location.World", loc.getWorld().getName());
		Main.config.set("location.X", loc.getX());
		Main.config.set("location.Y", loc.getY());
		Main.config.set("location.Z", loc.getZ());
		Main.config.set("location.Yaw", loc.getYaw());
		Main.config.set("location.Pitch", loc.getPitch());
		
		player.setGameMode(GameMode.ADVENTURE);
		inv.clear();
		inv.setHelmet(air);
		inv.setChestplate(air);
		inv.setLeggings(air);
		inv.setBoots(air);
		player.setHealth(20.0);
		player.setFoodLevel(20);
		
		inv.setHeldItemSlot(0);
		inv.setItemInHand(menu);
		inv.setHeldItemSlot(1);
		inv.setItemInHand(cosmetics);
		inv.setHeldItemSlot(2);
		inv.setItemInHand(settings);
		inv.setHeldItemSlot(8);
		inv.setItemInHand(hub);
		inv.setHeldItemSlot(0);
		
		e.setJoinMessage(API.getChatAPI().prefix("bonus", player.getName()) + API.getChatAPI().prefix("main", player.getName()) + API.getChatAPI().namecolor(player.getName()) + player.getName() + " §6joined the game.");
		Scoreboard.set(player);
	}
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) throws Exception {
		Player player = e.getPlayer();
			e.setQuitMessage(API.getChatAPI().colorize(API.getChatAPI().prefix("bonus", player.getName())) + API.getChatAPI().prefix("main", player.getName()) + API.getChatAPI().namecolor(player.getName()) + player.getName() + " §6left the game.");

	}
	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if (e.getCause() == DamageCause.VOID && e.getEntity() instanceof Player) {
			Location loc = new Location(Bukkit.getWorld("world"), 0.5, 65, 0.5);
			loc.setYaw(-180); loc.setPitch(0);
			e.getEntity().teleport(loc);
			e.setCancelled(true);
		} else {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onHungerChange(FoodLevelChangeEvent e) {
			e.setCancelled(true);
	}

}
