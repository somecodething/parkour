package org.MC3.parkour;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
/*import org.bukkit.World;*/
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

import me.Swedz.api.API;

public class Parkour implements Listener {
	public static HashMap<Player, Long> start = new HashMap<Player, Long>();

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		Action a = e.getAction();
		Player player = e.getPlayer();
		Location loc = player.getLocation();
		/*Location location = new Location((World) Main.config.get("location.World"), Main.config.getDouble("location.X"),
				Main.config.getDouble("location.Y"), Main.config.getDouble("location.Z"),
				(float) Main.config.getDouble("location.Yaw"), (float) Main.config.getDouble("location.Pitch"));*/

		if (a.equals(Action.PHYSICAL)) {
			if (e.getClickedBlock().getType() == Material.IRON_PLATE) {

				if (start.get(player) == null) {
					start.put(player, System.currentTimeMillis());
					player.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "You have started the parkour.");
					new BukkitRunnable() {
						public void run() {
							long time = System.currentTimeMillis() - start.get(player);
							String cT = ChatColor.AQUA + API.getMathAPI().secondsToString(time);
							Main.sendActionBar(player, ""+ChatColor.AQUA + ChatColor.BOLD+ "Time: " + cT);
							if (!start.containsKey(player)) {
								Main.sendActionBar(player, "§b");
								this.cancel();
							}
						}
					}.runTaskTimer(Main.instance, 0, 10);

					Main.config.set("location.World", loc.getWorld().getName());
					Main.config.set("location.X", loc.getX());
					Main.config.set("location.Y", loc.getY());
					Main.config.set("location.Z", loc.getZ());
					Main.config.set("location.Yaw", loc.getYaw());
					Main.config.set("location.Pitch", loc.getPitch());
					
				} else if (start.get(player) != null) {
					long time = System.currentTimeMillis() - start.get(player);
					if (time > 2000) {						
						start.remove(player);
						Main.sendActionBar(player, "§b");
						player.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "Your parkour time has reset.");
						start.put(player, System.currentTimeMillis());
						new BukkitRunnable() {
							public void run() {
								String cT = ChatColor.AQUA + API.getMathAPI().secondsToString(time);
								Main.sendActionBar(player, ""+ChatColor.AQUA + ChatColor.BOLD+ "Time: " + cT);
								if (!start.containsKey(player)) {
									Main.sendActionBar(player, "§b");
									this.cancel();
								}
							}
						}.runTaskTimer(Main.instance, 0, 10);
					}
				}
			}

			if (e.getClickedBlock().getType() == Material.GOLD_PLATE) {
				Location spawn = new Location(Bukkit.getWorld("World"), 0.5, 65, 0.5);
				spawn.setYaw(-180); spawn.setPitch(0);

				if (start.get(player) != null) {
					player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "You have completed the Parkour!");
					start.remove(player);
					player.teleport(spawn);
					Main.config.set("location.World", loc.getWorld().getName());
					Main.config.set("location.X", loc.getX());
					Main.config.set("location.Y", loc.getY());
					Main.config.set("location.Z", loc.getZ());
					Main.config.set("location.Yaw", loc.getYaw());
					Main.config.set("location.Pitch", loc.getPitch());
				}
			}
		}
	}
}
