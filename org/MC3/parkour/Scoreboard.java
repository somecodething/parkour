package org.MC3.parkour;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.ScoreboardManager;

import me.Swedz.api.API;

public class Scoreboard {
	public static void set(Player player) {
		ScoreboardManager manager = Bukkit.getScoreboardManager();
		org.bukkit.scoreboard.Scoreboard board = manager.getNewScoreboard();
	
		new BukkitRunnable(){
			@Override
			public void run(){
				Objective sb = board.registerNewObjective(player.getName(), "dummy");
				sb.setDisplayName("§e§lMCCUBED");
				sb.setDisplaySlot(DisplaySlot.SIDEBAR);
				
				sb.getScore("Server: §b#" + Bukkit.getServerName().replace("Parkour-", "")).setScore(12);
				sb.getScore("§1").setScore(11);
				try {
					sb.getScore("Rank: §b" + API.getRankManagement().getRank(player.getName())).setScore(10);
					sb.getScore("Gold: §b" + API.getGoldAPI().getGold(player.getName())).setScore(8);
					sb.getScore("Level: §b" + API.getLevelAPI().getLevel(player)).setScore(7);
					sb.getScore("§b" + API.getLevelAPI().getXP(player)).setScore(5);
				} catch (Exception e) {
					e.printStackTrace();
				}
				sb.getScore("§2").setScore(9);
				sb.getScore("XP Progress: ").setScore(6);
				sb.getScore("§3").setScore(4);
				sb.getScore("Players: §b" + Bukkit.getOnlinePlayers().size() + "/" + Bukkit.getMaxPlayers()).setScore(3);
				sb.getScore("§4").setScore(2);
				sb.getScore("§amc-cubed.org").setScore(1);
			
				player.setScoreboard(board);
			}
		}.runTaskAsynchronously(Main.instance);
	}
}